// Implementazione dell'algoritmo Greedy di Base in Java

import java.io.*;
import java.util.*;
import java.util.LinkedList;
import java.util.ArrayList;
 
class GraphGreedy {
    // Numero di vertici di un grafo
    private int V; 
    
    // Array contenente la lista di adiacenza di ciascun vertice
    private ArrayList<LinkedList<Integer>> adj;
    
    // Creazione del grafo, viene salvato il numero dei vertici e per ciascuno di essi viene creata la relativa lista di adiacenza
    GraphGreedy(int v) {
        V = v;
        adj = new ArrayList<LinkedList<Integer>>();
        for (int i = 0; i < v; ++i)
            adj.add(new LinkedList<Integer>());
    }
 
    // Aggiunta di un lato al grafo
    void addEdge(int v,int w) {
        adj.get(v).add(w);
        adj.get(w).add(v);
    }
 
    // Implementazione dell'Algoritmo Greedy di Base
    void greedyColoring() {
        // Array usato per salvare i colori di ciascun vertice
        int result[] = new int[V];
        
        // All'inizio dell'esecuzione tutti i vertici non sono colorati, questo si ottiene riempiendo l'array con il valore -1
        Arrays.fill(result, -1);
        
        // Al vertice di partenza viene assegnato il primo colore (0)
        result[0]  = 0;
        // Array di valori booleani che indica se l'i-esimo colore può essere usato per colorare un vertice oppure no
        boolean available[] = new boolean[V];
        // All'inizio dell'esecuzione si possono usare tutti i colori
        Arrays.fill(available, true);
        // Vengono assegnati i colori ai rimanenti V - 1 vertici
        for (int u = 1; u < V; u++) {
            // Vengono elaborati tutti i vertici adiacenti al vertice u e segnati i loro colori come non disponibili per il vertice u
            Iterator<Integer> it = adj.get(u).iterator();
            while (it.hasNext()) {
                int i = it.next();
                if (result[i] != -1)
                    available[result[i]] = false;
            }
            // Viene trovato il primo colore disponibile per il vertice utilizzato
            int cr;
            for (cr = 0; cr < V; cr++){
                if (available[cr])
                    break;
            }
            // Si assegna il colore trovato al vertice u
            result[u] = cr;
            // Si possono utilizzare nuovamente tutti i colori per il vertice successivo
            Arrays.fill(available, true);
        
        }
        // Stampa dei risultati ottenuti
        for (int u = 0; u < V; u++)
            System.out.println("Vertex " + u + " --->  Color "+ result[u]);
    }
 
    // Creare due grafi e utilizzare il metodo descritto precedentemente per colorare i loro vertici 
    public static void main(String args[]) {
        GraphGreedy g1 = new GraphGreedy(5);
        g1.addEdge(0, 1);
        g1.addEdge(0, 2);
        g1.addEdge(1, 2);
        g1.addEdge(1, 3);
        g1.addEdge(2, 3);
        g1.addEdge(3, 4);
        System.out.println("Coloring of graph 1");
        g1.greedyColoring(); 
        
        System.out.println();
        
        GraphGreedy g2 = new GraphGreedy(5);
        g2.addEdge(0, 1);
        g2.addEdge(0, 2);
        g2.addEdge(1, 2);
        g2.addEdge(1, 4);
        g2.addEdge(2, 4);
        g2.addEdge(4, 3);
        System.out.println("Coloring of graph 2 ");
        g2.greedyColoring();
    }
}
