% //////////////////////////////// Introduzione ////////////////////////////////////////

\begin{frame}{Introduzione}
    \framesubtitle{Obiettivi}  
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Definire i concetti, le proprietà, ed alcuni teoremi legati alla \emph{Teoria Cromatica dei Grafi} 
        \item \large Spiegare il funzionamento dei principali algoritmi utilizzati per la \emph{Colorazione di Grafi}
        \item \large Mostrare come risolvere il gioco del \emph{Sudoku} sfruttando gli algoritmi scelti
    \end{itemize}  
\end{frame}

\begin{frame}{Introduzione}
    \framesubtitle{Colorazione di Grafi}  
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large La \emph{Colorazione di Grafi} è un caso particolare di \emph{Etichettatura di Grafi}
        \item \large L'\emph{Etichettatura di Grafi} consiste nell'associare delle \emph{etichette} ai vertici o ai lati di un grafo
        \item \large Le etichette sono generalmente degli interi e nella \emph{Teoria Cromatica} prendono il nome di \emph{colori}        
    \end{itemize}  
\end{frame}


\begin{frame}{Introduzione}
    \framesubtitle{Colorazione dei Vertici}
    
     \begin{block}{Definizione} 
      \large Sia $G$ un grafo e $C$ un insieme finito di colori. Una \emph{colorazione} di $G$ è una funzione $c: V(G) \rightarrow C$ tale che $c(x) \neq c(y)$ con $ x,y \in V(G), (x,y) \in E(G)$
  \end{block}\pause 
  
  \bigskip 
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large $V(G)$ è l'insieme dei vertici del grafo $G$
        \item \large $E(G)$ identifica l'insieme dei lati di $G$
        \item \large L'insieme dei colori $C$ contiene le etichette che si vogliono associare ai vertici di un grafo   
    \end{itemize}  
\end{frame}



% //////////////////////////////// Polinomio Cromatico /////////////////////////////////

\begin{frame}[c]
    \centering
    \bigskip \bigskip \smallskip   
    \Huge Polinomio Cromatico
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Polinomio Cromatico}
    
     \begin{block}{Definizione} 
      \large Sia $k$ la cardinalità dell'insieme di colori $C$, il \emph{Polinomio Cromatico} $P_G(k)$ di un grafo $G$ conta le funzioni $c: V(G) \rightarrow C$ tali che $c(x) \neq c(y)$ con $ x,y \in V(G), (x,y) \in E(G)$
  \end{block}\pause 
  
  \bigskip 
    
    \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Il polinomio cromatico risponde alla domanda: \say{In quanti modi diversi può essere colorato un grafo $G$?}
        \item \large Le colorazioni di un grafo dipendono dal numero di colori $k$ che si hanno a disposizione
    \end{itemize}  
\end{frame}


\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Proprietà}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Il polinomio cromatico di un grafo $G$ è sempre \emph{omogeneo}, ha termine noto nullo, infatti non si possono fare colorazioni senza avere colori a disposizione
            \begin{equation*}
                P_G(0) = 0
            \end{equation*}
        \item \large Alcuni polinomi non sono cromatici per motivi strutturali
            \setbeamertemplate{itemize items}[square]
            \bigskip
            \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large $-3k^2 - k$ ha sempre soluzioni negative $\forall k > 0$
                \item \large $1.5k + 1.1k^2 + k^3$ ammette soluzioni non intere $\forall k > 0$            
            \end{itemize}            
    \end{itemize} 
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Proprietà}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Altri non lo sono per motivi concettuali
        \bigskip
        \setbeamertemplate{itemize items}[square]
        \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Il polinomio cromatico $P(k) = k + k^2 + ... + k^n$ con $n > 0$ non è associato a nessun grafo con almeno un lato
                \item \large Si supponga per assurdo che esista un grafo $H$ di cui $P_H(k)$ sia il polinomio cromatico e $(x,y) \in E(H)$ un suo lato
                \item \large $H$ non può essere colorato con un solo colore, infatti ogni colorazione assegnerebbe ai vertici $x$ e $y$ lo stesso colore
                \item \large Per $k = 1$, $P_H(k)$ dovrebbe essere uguale a zero, in realtà $P_H(k) = n \neq 0$: una contraddizione
        \end{itemize}
    \end{itemize} 
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Calcolo del Polinomio Cromatico}
    
  \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Vengono considerati grafi che non presentano \emph{cappi} e lati multipli: i cosiddetti \emph{grafi semplici} 
        \item \large I grafi analizzati sono mostrati in ordine crescente di vertici e di complessità
        \item \large Viene calcolato il polinomio cromatico di alcuni degli elementi caratterizzanti di un grafo     
    \end{itemize}    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo $I_0$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $I_0(V(I_0), E(I_0))$ un grafo privo di vertici: il cosiddetto grafo \emph{vuoto}
        \item \large $V(I_0) = \emptyset$ e $E(I_1) = \emptyset$
        \item \large Non avendo vertici il grafo $I_0$ può essere colorato in unico modo: non gli viene assegnato nessun colore
        \bigskip
        \begin{equation*}
          P_{I_0}(k) = 1
        \end{equation*}           
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo $I_1$}
    \bigskip \bigskip
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $I_1(V(I_1), E(I_1))$ un grafo costituito da un unico vertice 
        \item \large $V(I_1) = \{ u \}$ e $E(I_1) = \emptyset$
        \item \large Siccome l'insieme dei lati $E(I_1)$ è vuoto, il vertice $u$ risulta isolato e può essere colorato in $k$ modi diversi
        \bigskip
        \begin{equation*}
          P_{I_1}(k) = k
        \end{equation*}           
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo $I_1$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{I1}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo $I_2$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $I_2(V(I_2), E(I_2))$ un grafo costituito da due vertici isolati 
        \item \large $V(I_2) = \{ u, v \}$ e $E(I_2) = \emptyset$
        \item \large Siccome l'insieme dei lati $E(I_2)$ è vuoto, ciascun vertice può essere colorato in $k$ modi diversi
        \bigskip
        \begin{equation*}
          P_{I_2}(k) = k \cdot k = k^2
        \end{equation*}           
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo $I_2$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{I2}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo $K_2$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $K_2(V(K_2), E(K_2))$ un grafo costituito da due vertici collegati da un lato
        \item \large $V(K_2) = \{ u, v \}$ e $E(K_2) = \{ (u,v) \}$
        \item \large A prescindere dal vertice di partenza, il primo può essere colorato in $k$ modi, mentre al secondo restano $(k - 1)$ colori tra cui scegliere
        \bigskip
        \begin{equation*}
          P_{K_2}(k) = k \cdot (k -1) = (k)_2
        \end{equation*}           
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo $K_2$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{K2}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo $I_3$ e $K_3$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Un grafo $I_3$ è composto da tre vertici isolati, ciascuno dei quali può essere colorato in $k$ modi diversi
        \begin{equation*}
          P_{I_3}(k) = k \cdot k \cdot k = k^3
        \end{equation*}
        \item \large Un grafo $K_3$ è composto da tre vertici collegati tra loro a formare un triangolo
        \item \large A prescindere dal vertice di partenza, il primo può essere colorato in $k$ modi, il secondo in $(k - 1)$, mentre al terzo restano $(k -2)$ colori tra cui scegliere
        \bigskip
        \begin{equation*}
          P_{K_3}(k) = k \cdot (k -1) \cdot (k - 2) = (k)_3
        \end{equation*}          
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo $I_3$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{I3}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo $K_3$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{K3}
  \end{center}
\end{frame}


\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo Nullo $I_n$}
    
   \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Un \emph{grafo nullo} $I_n(V(I_n), E(I_n))$ è formato da $n$ vertici isolati 
        \item \large $V(I_n) = \{v_i : i = 1 \cdots n \}$ e $E(I_n) = \emptyset$
        \item \large Siccome l'insieme dei lati $E(I_n)$ è vuoto, ciascun vertice può essere colorato in $k$ modi diversi
        \bigskip
        \begin{equation*}
          P_{I_n}(k) = k_1 \cdot k_2 \cdots k_n = k^n
        \end{equation*}           
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo Nullo $I_n$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{In}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Grafo Completo $K_n$}
    
    \bigskip
    
    \begin{itemize} [<+->]
        \setlength\itemsep{1.2em}
        \item \large Un \emph{grafo completo} o \emph{cricca} $K_n(V(K_n), E(K_n))$ è composto da $n$ vertici completamente interconnessi tra loro 
        \item \large $V(K_n) = \{v_i : i = 1 \cdots n \}$
        \item \large \smallskip$E(K_n) = \{(u,v) \,\, | \,\, \forall u,v \in V(K_n) \}$  
        \item \large Il polinomio cromatico di $K_n$ viene calcolato con lo stesso procedimento utilizzato per $K_2$ e $K_3$
        \item \large A prescindere dal vertice di partenza, il primo può essere colorato in $k$ modi, il secondo in $(k - 1)$, il terzo in $(k - 2)$, \dots, mentre l'ultimo in $(k - n + 1)$
        \smallskip
        \begin{equation*}
          P_{K_n}(k) = k \cdot (k -1) \cdot (k - 2) \cdots (k - n + 1)  = (k)_n
        \end{equation*}  
               
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Grafo Completo $K_n$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{Kn}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Considerazioni}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Il polinomio cromatico del grafo vuoto $P_{I_0}(k)$ può essere ricavato a partire da quello dal grafo $I_n$
        \bigskip
        \begin{equation*}
          P_{I_0}(k) = k^0 = 1
        \end{equation*}
        \item \large I grafi completi $K_n$ hanno per polinomio cromatico il \emph{fattoriale decrescente} $(k)_n$              
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Cammino $P_n$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $P_n(V(P_n),E(P_n))$ un \emph{cammino} composto da $n$ vertici distinti  
        \item \large $V(P_n) = \{v_i : i = 1 \cdots n \}$
        \item \large $E(P_n) = \{(v_1,v_2),(v_2,v_3), \cdots ,(v_{n - 1},v_n) \}$
        \item \large Il primo vertice di $P_n$ può essere colorato in $k$ modi, il secondo in $(k - 1)$, il terzo ancora in $(k - 1)$ e così via fino all'ultimo vertice
        \smallskip
        \begin{equation*}
          P_{P_n}(k) = k \cdot (k - 1)_1 \cdot (k - 1)_2 \cdots (k - 1)_n  = k \cdot (k - 1)^{n-1}
        \end{equation*}             
    \end{itemize}
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Cammino $P_n$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{Pn}
  \end{center}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Circuito $C_n$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Sia $C_n(V(C_n),E(C_n))$ un \emph{circuito} composto da $n$ vertici distinti
        \item \large $V(C_n) = \{v_i : i = 1 \cdots n \}$
        \item \large $E(C_n) = \{(v_1,v_2), \cdots ,(v_{n - 1},v_n), (v_n,v_1) \}$
        \item \large Per poter calcolare il polinomio cromatico di un circuito $C_n$ vengono sfruttate le tecniche di \emph{delezione} e \emph{contrazione} di un lato del grafo    
    \end{itemize}    
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Circuito $C_n$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{Cn}
  \end{center}
\end{frame}


\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Delezione e Contrazione}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large La \emph{delezione} $G - (u,v)$ è una tecnica che consente di eliminare da un grafo $G$ il lato $(u,v) \in E(G)$ 
        \item \large La \emph{contrazione} $G / (u,v)$ è una tecnica utilizzata per contrarre i vertici $u,v \in V(G)$ di un grafo $G$. L'operazione di contrazione avviene in due passi
        \setbeamertemplate{itemize items}[square]
        \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Eliminare i vertici $u,v \in V(G)$ e tutti i lati ad essi connessi
                \item \large Aggiungere un nuovo vertice $uv$ all'insieme $V(G)$ e collegarlo a tutti i vicini dei vecchi vertici $u$ e $v$
        \end{itemize}
    \end{itemize}    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Formula Ricorsiva}
    
    \begin{block}{Definizione} 
      \large Dato un grafo $G$, per ogni lato $(u,v) \in E(G)$
      \begin{equation*}
          P_G(k) = P_{G - (u,v)}(k) - P_{G / (u,v)}(k)
      \end{equation*}
  \end{block}\pause
  
  \bigskip
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}        
        \item \large $P_{G - (u,v)}(k)$ comprende tutte le colorazioni in cui $u$ ha lo \emph{stesso} colore di $v$ più tutte quelle in cui il colore di $u$ è \emph{diverso} da quello da $v$ 
        \item \large $P_{G / (u,v)}(k)$ comprende tutte le colorazioni in cui $u$ ha lo \emph{stesso} colore di $v$
    \end{itemize}    
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Esempio Formula Ricorsiva}
    
   \begin{center}
      \includegraphics[scale = 0.5]{esempio}
  \end{center}
  
  \vspace{-1.8em}
  
  \large 
  \begin{align*}
       P_{K_2}(k) &= P_{I_2}(k) - P_{I_1}(k) \\[10pt]
                 &= k^2 - k \\[10pt]
                 &= k \cdot (k - 1) 
  \end{align*}   
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Circuito $C_n$}
    
   \begin{center}
      \includegraphics[scale = 0.5]{circuito}
  \end{center}
  
  \large 
  \begin{align*}
          P_{C_n}(k) &= P_{C_n - (u,v)}(k) - P_{C_n / (u,v)}(k) \\[10pt]
                     &= P_{P_n}(k) - P_{C_{n - 1}}(k)
  \end{align*}
  
    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Circuito $C_n$}
    
    \bigskip 
    
    \begin{block}{Proposizione} 
      \large Dato un circuito $C_n$ di ordine $n \geq 3$
      \begin{equation*}
          P_{C_n}(k) = (k - 1)^n + (-1)^n \cdot (k - 1)
      \end{equation*}
  \end{block}\pause
  
  \smallskip
  
  \begin{itemize} [<+->]
        \setlength\itemsep{1em}        
        \item \large A partire dalla formula ricorsiva viene dimostrata la \emph{proposizione} per induzione
        \item \large \emph{Base Induttiva}: $n = 3$
        \begin{align*}
           P_{C_3}(k) &= k \cdot (k - 1) \cdot (k - 2) \\[8pt]
                      &= k^3 -3k^2 + 2k \\[8pt]
                      &= (k - 1)^{3} - (k - 1)           
         \end{align*}        
    \end{itemize}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Circuito $C_n$}
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}        
        \item \large \emph{Passo Induttivo}: si vuole dimostrare che $\forall n > 3$ il polinomio cromatico
        \begin{align*}
           P_{C_{n + 1}}(k) &= P_{C_{n + 1} - (u,v)}(k) - P_{C_{n + 1} / (u,v)}(k) \\[10pt]
                           &= (k - 1)^{n + 1} + (-1)^{n + 1} \cdot (k - 1)                      
         \end{align*}
          \item \large Il minuendo $P_{C_{n + 1} - (u,v)}(k)$        
           \begin{align*}
               P_{C_{n + 1} - (u,v)}(k) = P_{P_{n + 1}}(k) = k \cdot (k - 1)^n
           \end{align*}
         \item \large Il sottraendo $P_{C_{n + 1} / (u,v)}(k)$    
           \begin{align*}
               P_{C_{n + 1} / (u,v)}(k) = P_{C_{n}}(k) = (k - 1)^n + (-1)^n \cdot (k - 1)
           \end{align*}
              
  \end{itemize}
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Circuito $C_n$}
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}        
        \item \large Si verifica che la proposizione è vera $\forall n > 3$
            \begin{align*}
               P_{C_{n + 1}}(k) &= P_{C_{n + 1} - (u,v)}(k) - P_{C_{n + 1} / (u,v)}(k) \\[10pt]
                                         &= k \cdot (k - 1)^n - (k -1)^n - (-1)^n \cdot (k - 1) \\[10pt]
                                         &= (k - 1)^n \cdot (k - 1) + (-1)^{n + 1} \cdot (k - 1) \\[10pt]
                                         &= (k -1)^{n + 1} + (-1)^{n + 1} \cdot (k - 1)
            \end{align*}              
  \end{itemize}
\end{frame}


\begin{comment} 

\begin{align*}
       P_{C_n}(k) &= P_{P_n}(k) - P_{C_{n-1}}(k) \\[10pt]
                  &= P_{P_n}(k) - ( P_{P_{n-1}}(k) - P_{C_{n-2}}(k) )         \\[10pt]
                  &= P_{P_n}(k) - P_{P_{n-1}}(k) + P_{C_{n-2}}(k)             \\
                  & \vdotswithin{=} \\
                  &= P_{P_n}(k) - P_{P_{n-1}}(k) + \cdots + (-P_{P_2}(k))     \\[10pt]
                  &= k \cdot (k - 1)^{n - 1} - k \cdot (k - 1)^{n-2} + \cdots + (-k \cdot (k -1))
  \end{align*}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Esempio circuito $C_4$}
    
    \begin{center}
      \includegraphics[scale = 0.4]{circuito4}
  \end{center}
  
  \vspace{-3em}
  
  %\large 
  \begin{align*}
       P_{C_4}(k) &= P_{I_4}(k) -4 \cdot P_{I_3}(k) + 6 \cdot P_{I_2}(k) -3 \cdot P_{I_1}(k)   \\[10pt]
                  &= k^4  - 4k^3 +6k^2 -3k        \\[10pt]
                  &= (k -1)^4 + (k - 1)
  \end{align*}   
\end{frame}  


\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Teorema di Whitney}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Fissato un grafo semplice $G$
         \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Dare un ordinamento arbitrario ai suoi lati
                \item \large Elencare i circuiti che lo compongono 
                \item \large Ricavare l'insieme dei \emph{circuiti spezzati} rimuovendo il lato con il valore maggiore da ciascuno dei suoi circuiti
          \end{itemize}
        \item \large Attenzione: un circuito spezzato \emph{non} è un circuito       
    \end{itemize}    
\end{frame}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Teorema di Whitney}
    
    \begin{block}{Definizione} 
      \large Per ogni grafo semplice $G$ di ordine $n$
      \begin{equation*}
          P_G(k) = \sum_{m = 0}^{n} (-1)^m d_{n,m} t^{n - m}
      \end{equation*}
  \end{block}\pause 
  
  \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large I coefficienti $d_{n,m}$ del polinomio cromatico $P_G(k)$ rappresentano il numero di sottoinsiemi di lati di ordine $n - m$ che \emph{non} contengono circuiti spezzati               
    \end{itemize}  
\end{frame}

\end{comment}

\begin{frame}{Polinomio Cromatico}
    \framesubtitle{Albero $T_n$}
    
    \begin{itemize} [<+->]
        \setlength\itemsep{3em}
        \item \large Un albero $T_n$ è un grafo connesso aciclico formato da $n$ vertici distinti e $n - 1$ lati  
       %\item \large La \emph{radice} $r$ di un albero $T_n$ è un vertice che consente di raggiungere tutti gli altri vertici usando uno ed un solo cammino
       \item \large Ciascun vertice $v$ fa parte di un cammino che può partire da un qualsiasi vertice dell'albero
        %\item \large La radice $r$ può essere colorata in $k$ modi diversi, mentre i restanti $n - 1$ vertici hanno a disposizione $(k - 1)$ colori
        \bigskip
        \begin{equation*}
          P_{T_n}(k) = k_r \cdot (k - 1)_{v_1} \cdot (k - 1)_{v_2} \cdots (k - 1)_{v_n}  = k \cdot (k - 1)^{n-1}
        \end{equation*}             
    \end{itemize}
\end{frame}

\begin{frame}{Polinomio Cromatico}
  \framesubtitle{Albero $T_n$}
  
  \begin{center}
      \includegraphics[scale = 0.7]{Tn}
  \end{center}
\end{frame}

% //////////////////////////////// Numero Cromatico /////////////////////////////////

\begin{frame}[c]
    \centering
    \bigskip \bigskip \smallskip   
    \Huge Numero Cromatico
\end{frame}



\begin{frame}{Numero Cromatico}
    \framesubtitle{k-colorazioni}   
    
    \begin{block}{Definizione} 
      \large Un grafo $G$ è \emph{k-colorabile} se esiste una \emph{colorazione} di $G$ in $k$ colori 
  \end{block}\pause
  
  \bigskip
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}        
        \item \large Il polinomio cromatico $P_G(k)$ conta il numero delle diverse \emph{k-colorazioni} di un grafo $G$
        \item \large Un grafo $G$ è k-colorabile se e solo se $P_G(k) > 0$
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Il circuito $C_3$ non può essere colorato con soli due colori
                \item \large $P_{C_3}(2) = 2 \cdot (2 -1) \cdot (2 -2) = 0$                
          \end{itemize}  
    \end{itemize}
\end{frame}
    
\begin{frame}{Numero Cromatico}
    \framesubtitle{Numero Cromatico}   
    
    \begin{block}{Definizione} 
      \large Il \emph{numero cromatico} $\chi(G)$ di un grafo $G$ è il \emph{minimo} $k$ per cui $G$ è \emph{k-colorabile}
  \end{block}\pause
  
  \bigskip
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em} 
        \item \large $\chi(G)$ è il più piccolo $k$ tale per cui $P_G(k) > 0$       
        \item \large $\chi(G) > k \implies P_G(k) = 0$
        \item \large $\chi(G) \leq k \implies P_G(k) > 0$ 
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Osservazioni}   
   
  \begin{itemize} [<+->]
        \setlength\itemsep{2em} 
        \item \large Un grafo completo $K_n$ ha un numero cromatico $\chi(K_n)$ pari a $n$ in quanto ciascun vertice $u$ è adiacente agli $n - 1$ vertici restanti 
        \item \large Un circuito $C_n$ con $n > 1$ e $n = 2t, t \in \mathbb{N}$ ha un numero cromatico $\chi(C_n)$ pari a 2
        \item \large Un circuito $C_n$ con $n > 1$ e $n = 2t +1, t \in \mathbb{N}$ ha un numero cromatico $\chi(C_n)$ pari a 3
        \item \large In genere è piuttosto difficile determinare il numero cromatico di un grafo semplice $G$, per questo motivo si tende a cercare un \emph{lower bound} ed un \emph{upper bound} per $\chi(G)$              
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Osservazioni}
    
    
   \begin{columns}
        \begin{column}{0.5\textwidth}
            \begin{center}
              \includegraphics[scale = 0.39]{Kn}      
            \end{center}
            
            \vspace{-2.2em}
  
            \begin{equation*}
              \chi(K_n) = n
            \end{equation*}        
        \end{column}
        \begin{column}{0.5\textwidth}
            \begin{center}
              \includegraphics[scale = 0.39]{C4}      
            \end{center}
            
            \vspace{-2.2em}
  
            \begin{equation*}
              \chi(C_4) = 2
            \end{equation*} 
        \end{column}
    \end{columns}
  
  \begin{center}
      \includegraphics[scale = 0.39]{Cn}      
  \end{center}
  
  \vspace{-2.7em}
  
  \begin{equation*}
      \chi(C_5) = 3
  \end{equation*}    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Densità di un grafo semplice}
    
    \bigskip   
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large Sia $K(X(G), Y(G))$ una cricca di un grafo semplice \\ \medskip $G(V(G),E(G))$
        \item \large $X(G) \subseteq V(G)$
        \item \large $Y(G) = \{(u,v) \,\, | \,\, \forall u,v \in X(G) \}$           
        \item \large La \emph{densità} di un grafo semplice $G$ è la \emph{massima} cardinalità di una \\ \smallskip cricca $K$ di $G$  
        \bigskip
        \begin{equation*}
            de(G) = max \{ |X(G)| : X(G) \subseteq V(G), K  \} 
        \end{equation*}             
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Lower Bound}
    
    \begin{block}{Proposizione} 
      \large Per ogni grafo semplice $G$
      \vspace{-0.7em}
      \begin{equation*}
          \chi(G) \geq de(G)
      \end{equation*}
  \end{block}\pause
    
    \bigskip   
  
  \begin{itemize} [<+->]
        \setlength\itemsep{2em}
        \item \large \emph{Dimostrazione}
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Il numero cromatico $\chi(G)$ di una cricca $K_n$ è pari al numero di vertici $n$ che la compongono
                \item \large È evidente che se in $G$ esiste una cricca $K_n$ con densità pari a $de(G)$
                \begin{equation*}
                  \chi(G) \geq de(G)
                \end{equation*}                 
          \end{itemize}          
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Upper Bound}
    
    \begin{block}{Proposizione} 
      \large Per ogni grafo semplice $G$
      \vspace{-0.7em}
      \begin{equation*}
          \chi(G) \leq \Delta(G) + 1
      \end{equation*}
  \end{block}\pause
    
    \bigskip   
  
  \begin{itemize} [<+->]
        \setlength\itemsep{1em}
        \item \large $\Delta(G)$ corrisponde al \emph{grado massimo} di un grafo semplice $G$
        \begin{equation*}
         \Delta(G) = max \{ d(u) \,\, | \,\, u \in V(G) \}
        \end{equation*}
        \item \large Si vuole dimostrare la proposizione per induzione 
        \item \large \emph{Base Induttiva: $n = 1$}
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Dato $I_1$, allora $\chi(I_1) = 1$ e $\Delta(I_1) = 0$
                \bigskip
                \begin{equation*}
                  \chi(I_1) \leq \Delta(I_1) + 1 = 0 + 1 = 1
                \end{equation*}            
          \end{itemize}          
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Upper Bound} 
  
  \begin{itemize} [<+->]
        \setlength\itemsep{1em}
        \item \large \emph{Passo Induttivo}
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{3em}
                \item \large Si supponga che per qualsiasi grafo semplice $G_n$ con $n > 1$ valga la proposizione   
                \bigskip
                \begin{equation*}
                  \chi(G_n) \leq \Delta(G_n) + 1
                \end{equation*}
                \item \large Si vuole dimostrare che per ogni grafo semplice $G_{n + 1}$ con $ n > 1$ valga
                \bigskip
                \begin{equation*}
                  \chi(G_{n + 1}) \leq \Delta(G_{n + 1}) + 1
                \end{equation*}
                            
          \end{itemize}          
  \end{itemize}        
    
\end{frame}

\begin{frame}{Numero Cromatico}
    \framesubtitle{Upper Bound}
    
  \bigskip 
  
  \begin{itemize} [<+->]
        \setlength\itemsep{1em}
        \item \large \emph{Passo Induttivo}
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{2em}
                \item \large Sia $u$ un vertice di un grafo semplice $G_{n + 1}$
                \item \large Rimuovendo $u$ da $G_{n + 1}$ si ottiene
                \begin{equation*}
                  G_n =  G_{n + 1} - u
                \end{equation*}
                \item \large Sapendo per \emph{ipotesi induttiva} che
                \begin{equation*}
                  \chi(G_n) \leq \Delta(G_n) + 1
                \end{equation*}
                \item \large E che il grado del vertice $u$
                \begin{equation*}
                  d(u) \leq \Delta(G_{n + 1})
                \end{equation*}                            
          \end{itemize}          
  \end{itemize}        
    
\end{frame}


\begin{frame}{Numero Cromatico}
    \framesubtitle{Upper Bound} 
    
  \bigskip
  
  \begin{itemize} [<+->]
        \setlength\itemsep{1em}
        \item \large \emph{Passo Induttivo}
        \bigskip
        \setbeamertemplate{itemize items}[square]
         \begin{itemize} [<+->]
                \setlength\itemsep{3em}
                \item \large Reinserendo $u$ in $G_n$, $u$ può essere colorato in $\Delta(G_n) + 1$ modi diversi. Qualunque sia il colore scelto per $u$, corrisponderà ad uno dei differenti colori assegnati ai vertici del suo vicinato in quanto \\ \smallskip $d(u) \leq \Delta(G_{n + 1})$
                %\bigskip
                %\begin{equation*}
                   %c(u) = c(v), \forall v \in V(G_n) : (u,v) \in E(G_n)
                %\end{equation*}                              
                \item \large Proprio per questo è necessario aggiungere un ulteriore colore
                \bigskip
                \begin{equation*}
                  \chi(G_{n + 1}) \leq \Delta(G_{n + 1}) + 1
                \end{equation*}           
          \end{itemize}          
  \end{itemize}    
\end{frame}