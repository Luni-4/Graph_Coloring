import java.io.IOException;
import java.util.Iterator;

/**
 * Questa classe risolve il Sudoku utilizzando l'algoritmo Greedy di Base
 *
 */
public class SudokuProcessor {
	private SudokuGrid sudokuGrid;
	
	
	public SudokuProcessor (SudokuGrid sudokuGrid)	{
		this.sudokuGrid = sudokuGrid;
	}
	
	// Viene invocato il metodo per risolvere il Sudoku
	public void solvePuzzle() throws IndexOutOfBoundsException, IOException {
		
		if (solveWithRecursion(0, 0))	{
			sudokuGrid.setSolved(true);
		} else	{
			System.out.println("Sudoku non risolvibile.");
			System.exit(0);
		}
	}
	
	/**
	 * Per ciascuno dei vertici non colorati, l'algoritmo gli assegna il primo colore disponibile
	 * e verifica che non sia uguale ad uno dei suoi vertici adiacenti
	 * Nel caso fosse uguale, si passa al colore successivo
	 * Se non è possibile assegnarli un colore, il Sudoku non è risolvibile e la funzione termina
	 */
	private boolean solveWithRecursion(int x, int y) throws IOException {
		int valueOfCurrentNode;

		if (x == 9) {			
			x = 0;				
			if (++y == 9) {
				return true;
			}
		}

		valueOfCurrentNode = sudokuGrid.getNode(x, y).getValue(); 

		if (valueOfCurrentNode != 0) { 				// Se al vertice è assegnato un colore, passare al successivo
			return solveWithRecursion(x + 1, y);	
		}
		
		// Se ad un vertice non è assegnato nessun colore, provare ad assegnarne uno controllando che non sia uguale ad uno di quelli
		// della sua riga, colonna, sottogriglia (settore)
		// Se il colore è disponibile, assegnare al vertice il colore 
		for (int valueToInject = 1; valueToInject <= 9; ++valueToInject) {
			if (checkValueToInject(x, y, valueToInject)) { 			
				sudokuGrid.getNode(x, y).setValue(valueToInject); 	
				if (solveWithRecursion(x + 1, y)) {					
					return true;									
				}
			}
		} 
		sudokuGrid.getNode(x, y).setValue(0);	// Se non c'è nessun colore valido per il vertice, allora il Sudoku non può essere risolto
		return false;							// e la funzione termina con esito negativo												
	}
	
	// Verifica che il colore scelto per un vertice v non sia stato assegnato ad uno dei vertici che fanno parte della sua stessa riga, colonna e settore 
	private boolean checkValueToInject(int x, int y, int valueToCheck) {
		if (checkRow(valueToCheck, x, y)) { 																
			if (checkColumn(valueToCheck, x, y)) { 		
				if (checkSector(valueToCheck, x, y)) {  
					return true; 				
				}
			}
		}
		return false;		
	}

	// Controllare se il colore che si vuole utilizzare per un vertice v non sia stato già utilizzato da un vertice appartenente
	// alla stessa riga di v	
	private boolean checkRow(int valueToCheck, int X, int Y) {
		for (int x = 0; x < 9; x++) {
			if (sudokuGrid.getNode(x, Y).getValue() == valueToCheck) {
				return false;		
			}
		}
		return true;
	}
	
	// Controllare se il colore che si vuole utilizzare per un vertice v non sia stato già utilizzato da un vertice appartenente
	// alla stessa colonna di v
	private boolean checkColumn(int valueToCheck, int X, int Y) {
		for (int y = 0; y < 9; y++) {
			if (sudokuGrid.getNode(X, y).getValue() == valueToCheck) {
				return false;		
			}
		}
		return true;
	}
	
	// Controllare se il colore che si vuole utilizzare per un vertice v non sia stato già utilizzato da un vertice appartenente
	// alla stessa sottogriglia (settore) di v
	private boolean checkSector(int valueToCheck, int X, int Y) throws IndexOutOfBoundsException {
		Iterator<SudokuVertexNode> sectorToCheck = null;
		
		switch (sudokuGrid.getNode(X, Y).getSector())	{
		case 1:		sectorToCheck = sudokuGrid.getSector(1).iterator();
					break;
		case 2:		sectorToCheck = sudokuGrid.getSector(2).iterator();
					break;
		case 3:		sectorToCheck = sudokuGrid.getSector(3).iterator();
					break;
		case 4:		sectorToCheck = sudokuGrid.getSector(4).iterator();
					break;
		case 5:		sectorToCheck = sudokuGrid.getSector(5).iterator();
					break;
		case 6:		sectorToCheck = sudokuGrid.getSector(6).iterator();
					break;
		case 7:		sectorToCheck = sudokuGrid.getSector(7).iterator();
					break;
		case 8:		sectorToCheck = sudokuGrid.getSector(8).iterator();
					break;
		case 9:		sectorToCheck = sudokuGrid.getSector(9).iterator();
					break;
		default:	throw new IndexOutOfBoundsException("Impossibile individuare l'indice" + sectorToCheck);
		}
		

		while (sectorToCheck.hasNext())	{
			SudokuVertexNode nodeToCheck = sectorToCheck.next();
			if (nodeToCheck.getValue() == valueToCheck)	{
				return false;		
			}
		}
		return true;
	}
}
