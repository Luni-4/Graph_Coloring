/**
 * Questa classe permette di eseguire il risolutore di Sudoku che sfrutta l'algoritmo Greedy di Base
 * Come prima cosa viene creata la griglia 9 x 9 e popolata con i dati provenienti da un file di testo
 * Stampare lo schema iniziale
 * Viene invocata la funzione che individua la soluzione a partire dallo schema iniziale
 * Stampare la soluzione finale
 */
public class Run {
	
	public static void main(String[] args) throws Exception {
	    // Creare la griglia di gioco ed inserire i numeri iniziali
		SudokuGrid sudokuGrid = new SudokuGrid();
		// Creare la classe che consente di risolvere il sudoku utilizzando l'algoritmo Greedy di Base
		SudokuProcessor processor = new SudokuProcessor(sudokuGrid);
		
		// Stampare la griglia e lo stato iniziale del gioco
		sudokuGrid.printGrid();
		// Risolvere il gioco		
		processor.solvePuzzle();
		// Stampare la soluzione del Sudoku		
		sudokuGrid.printGrid();
		
		System.exit(0);
	}
}
