import java.io.BufferedReader;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Questa classe crea una griglia 9 x 9 e per ciascuna cella individua i suoi vicini
 * I vicini di una cella sono la cella superiore (nord), inferiore (sud), sinistra (ovest), destra (est)
 * Viene individuata la sottogriglia (settore) a cui è associata una cella
 * La cella di coordinate (0,0) è la cella più a sinistra della griglia partendo dall'alto
 */
public class SudokuGrid {

	int vertices = 0;
	private ArrayList<SudokuVertexNode> sector1, sector2, sector3, sector4,
			sector5, sector6, sector7, sector8, sector9;
	private boolean solved = false;
	
	public SudokuGrid() throws Exception	{
		initializeSectors();
		createGrid();
	}
	
	public void initializeSectors() {
		sector1 = new ArrayList<SudokuVertexNode>();
		sector2 = new ArrayList<SudokuVertexNode>();
		sector3 = new ArrayList<SudokuVertexNode>();
		sector4 = new ArrayList<SudokuVertexNode>();
		sector5 = new ArrayList<SudokuVertexNode>();
		sector6 = new ArrayList<SudokuVertexNode>();
		sector7 = new ArrayList<SudokuVertexNode>();
		sector8 = new ArrayList<SudokuVertexNode>();
		sector9 = new ArrayList<SudokuVertexNode>();
	}
	
	// Funzione utilizzate per creare la griglia
	private void createGrid() throws Exception {
		SudokuVertexNode temp = null;
		
		// Ciclare sulle colonne
		for (int y = 0; y < 9; y++) {
			// Ciclare sulle righe
			for (int x = 0; x < 9; x++) {
				SudokuVertexNode newNode = new SudokuVertexNode(x, y);
				
				switch (x) {
				case 0:
					switch (y) {
					case 0: // Viene individuato il primo nodo sulla griglia e vengono posti i suoi vicini a NULL
						newNode.setNorth(null);	
						newNode.setSouth(null);
						newNode.setEast(null);
						newNode.setWest(null);
						break;
						
					default: // Vengono individuati i vicini delle celle appartenenti alla prima colonna della griglia
						temp = getNode(x, y - 1); 		
						temp.setSouth(newNode); 		
						newNode.setNorth(temp); 		
						newNode.setSouth(null);
						newNode.setEast(null);
						newNode.setWest(null);
						break;
					}
					break;
				default:
					switch (y) {
					case 0: // Vengono individuati i vicini delle celle appartenenti alla prima riga della griglia
						temp = getNode(x - 1, y);		
						temp.setEast(newNode); 			
						newNode.setNorth(null);
						newNode.setSouth(null);
						newNode.setEast(null);
						newNode.setWest(temp); 			
						break;
						
					default: // Vengono individuati i vicini di tutte le altre celle della griglia
						temp = getNode(x - 1, y);		
						temp.setEast(newNode); 			
						newNode.setWest(temp); 			
						
						temp = getNode(x, y - 1);		
						temp.setSouth(newNode); 		
						newNode.setNorth(temp); 		
						
						newNode.setSouth(null);
						newNode.setEast(null);
						break;
					}
					break;
				}
				add(newNode); // Viene aggiunto il nodo alla griglia
			}
		}
		prePopulateNodes();		// Caricare i valori dello schema iniziale dal file di testo
								
	}
	
	public SudokuVertexNode getNode(int x, int y) {
		SudokuVertexNode nodeToReturn = sector1.get(0);
		
		if ((x == 0) && (y == 0)){
			return nodeToReturn;
		}
		
		// La ricerca di un nodo parte dalla posizione della griglia (0,0)
		for (int i = 0; i < x; i++) {
			nodeToReturn = nodeToReturn.getEast();
		}
		
		for (int k = 0; k < y; k++) {
			nodeToReturn = nodeToReturn.getSouth();
		}
		
		return nodeToReturn;
	}
	
	// Viene aggiunto un nodo ad una sottogriglia, chiamata in inglese sector (settore)
	// I settori sono contati a partire dalle righe
	public void addNodeToSector(SudokuVertexNode nodeToAdd) {
		int x = nodeToAdd.getX();
		int y = nodeToAdd.getY();

		switch(x) {
		case 0: case 1: case 2:
			switch(y) {
				case 0: case 1: case 2: nodeToAdd.setSector(1);
										sector1.add(nodeToAdd); 
										break;
				case 3: case 4: case 5: nodeToAdd.setSector(4);
										sector4.add(nodeToAdd); 
										break;
				case 6: case 7: case 8: nodeToAdd.setSector(7);
										sector7.add(nodeToAdd); 
										break;
			}
			break;
			
		case 3: case 4: case 5:
			switch(y) {
				case 0: case 1: case 2: nodeToAdd.setSector(2);
										sector2.add(nodeToAdd); 
										break;
				case 3: case 4: case 5: nodeToAdd.setSector(5);
										sector5.add(nodeToAdd); 
										break;
				case 6: case 7: case 8: nodeToAdd.setSector(8);
										sector8.add(nodeToAdd); 
										break;
			}
			break;
			
		case 6: case 7: case 8: 
			switch(y) {
				case 0: case 1: case 2: nodeToAdd.setSector(3);
										sector3.add(nodeToAdd); 
										break;
				case 3: case 4: case 5: nodeToAdd.setSector(6);
										sector6.add(nodeToAdd); 
										break;
				case 6: case 7: case 8: nodeToAdd.setSector(9);
										sector9.add(nodeToAdd); 
										break;
			}
			break;
		}
	}

	public int numVertices() {
		return vertices;
	}

	
	public void add(SudokuVertexNode nodeToAdd) {
		addNodeToSector(nodeToAdd);
		vertices++;
	}
	
	public void prePopulateNodes() throws Exception {

		BufferedReader sudokuValues = null;
		 
		try {

			int currentValue;
			sudokuValues = new BufferedReader(new FileReader(
					"txtSudoku/valori_sudoku.txt"));
			for (int x = 0, y = 0; y < 9; y++) {
				x = 0;
				for (; x < 9; x++) {
					do	{
						currentValue = Character.getNumericValue(sudokuValues
								.read());
					} while (currentValue == -1);
					
						getNode(x, y).setValue(currentValue);
				}
			}

		} catch (Exception e) {
			System.out.println("Errore nell'apertura del file.\n");		
		}
	}
	
	public void printGrid()	{
		
		if (solved) {
			System.out.println("   SOLUZIONE SUDOKU");
		} else {
			System.out.println("\n SCHEMA INIZIALE SUDOKU");
		}

		for	(int x = 0, y = 0; y < 9; y++)	{
			x = 0;
			if ((y % 3) == 0)	{
				System.out.println("-------------------------");
			}
			for (; x < 9; x++)	{
				
				if ((x % 3) == 0)	{
					System.out.print("| ");
				}
				switch (x){
					case 0:  System.out.print(getNode(x, y).getValue() + " ");
							 break;
					case 8:  System.out.print(getNode(x, y).getValue());
							 break;
					default: System.out.print(getNode(x, y).getValue() + " ");
							 break;
				}
			}
			System.out.println(" |");
		}
		System.out.println("-------------------------\n\n");
	}
	
	public ArrayList<SudokuVertexNode> getSector(int sector)	{
		
		switch (sector)	{
			case 1:	return sector1;
			case 2:	return sector2;
			case 3:	return sector3;
			case 4:	return sector4;
			case 5:	return sector5;
			case 6:	return sector6;
			case 7:	return sector7;
			case 8:	return sector8;
			case 9:	return sector9;
			default: System.out.println("Impossibile individuare il settore " + sector + ".");
					 System.exit(1);
		}
		return null;
	}
	
	public boolean isSolved() {
		return solved;
	}

	public void setSolved(boolean solved) {
		this.solved = solved;
	}
}
