import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

class Graph {

	private List<Vertex> vertices;
	
	public Graph(String _path){
		vertices = new ArrayList<Vertex>();
		ArrayList<String> lines = (ArrayList<String>) readGraphData(_path);
		
		if (lines != null){
			for (int i = 1; i < lines.size(); i++){
				String node = lines.get(i).split(":")[0];
				String[] adj = lines.get(i).split(":")[1].split(" ");
				
				List<String> neighbors = new ArrayList<String>();
				for (int j = 1; j < adj.length; j++){
					neighbors.add(adj[j]);
				}
				vertices.add(new Vertex(node, new ArrayList<String>(neighbors)));
			}	
		}
		
	}
	
	@Override
	public String toString() {
		String result = "";
		for (Vertex v: vertices){
			result += v.node + ":" + v.neighbors.toString() + "\n";
		}
		return result;
	}
    
    // Implementazione dell'algoritmo di Welsh-Powell
	public void colourVertices(){
		Collections.sort(vertices, new VertexComparator()); 
		Map<String, String> vertex_color_index = new HashMap<String, String>(); 
		for (int i = 0; i < vertices.size(); i++){
			if ((vertex_color_index.containsKey(vertices.get(i).node))){	
				continue;
			}
			else{
				vertex_color_index.put(vertices.get(i).node, "Colore " + i); 
				for (int j = i+1; j < vertices.size(); j++){
					if (!(vertices.get(i).neighbors.contains(vertices.get(j).node)) && !(vertex_color_index.containsKey(vertices.get(j).node))){
						vertex_color_index.put(vertices.get(j).node, "Colore " + i);
					}
					else{
						continue;
					}
				}
			}	
		}
		System.out.println(vertex_color_index);
		
	}
	
	private List<String> readGraphData(String _path){
		Path path = FileSystems.getDefault().getPath(_path, "");
		try {
			return Files.readAllLines(path, Charset.defaultCharset());
		} catch (IOException e) {
			System.err.println("Errore nella lettura del file.");
			return null;
		}
	}
	
	
	
	private static class Vertex{
		private String node;
		private List<String> neighbors;
		
		public Vertex(String node, ArrayList<String> neighbors){
			this.node = node;
			this.neighbors = neighbors;
		}
		
	}
	
	class VertexComparator implements Comparator<Vertex>{

		@Override
		public int compare(Vertex a, Vertex b) {
			return a.neighbors.size() < b.neighbors.size() ? 1 : a.neighbors.size() == b.neighbors.size() ? 0 : -1;
		}
		
	}
	
	public static void main(String[] args){
		Graph graph = new Graph("data.txt");
		graph.colourVertices();
	}
}
