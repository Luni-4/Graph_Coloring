Graph Coloring
================

# Indice

1. [OBIETTIVO](#1-obiettivo)
2. [STORIA](#2-storia)
3. [FORMULAZIONE MATEMATICA](#3-formulazione-matematica)
4. [ALGORITMI GENERICI](#4-algoritmi-generici)
5. [ESEMPIO DI APPLICAZIONE PRATICA](#5-esempio-di-applicazione-pratica)

-----------------

<!--- 6. [CONCLUSIONI](#6-conclusioni) --> 

# 1. OBIETTIVO

- Analisi della teoria cromatica dal punto di vista combinatorio e applicativo

# 2. STORIA

- Storia generale della colorazione dei grafi (sottosezione-wikipedia)

# 3. FORMULAZIONE MATEMATICA

- Definizione e terminologia del vertex coloring (sottosezione wikipedia)
    - Paper pag 91: https://drive.google.com/drive/folders/1FErMZvfUOdZovVUGiWbAhy9e7mjsD1RP
	- Dimostrazione di Brooks (pag 29 tesi)

- Cosa è il polinomio cromatico 
    - Introduzione: https://en.wikipedia.org/wiki/Chromatic_polynomial)
	- Tesi pag 41: https://drive.google.com/drive/folders/1FErMZvfUOdZovVUGiWbAhy9e7mjsD1RP

- Numero cromatico e proprietà 
    - Da pag 91 corso Teoria grafi: https://drive.google.com/drive/folders/1FErMZvfUOdZovVUGiWbAhy9e7mjsD1RP

- Teorema del 4 colori e dimostrazione di Kampè 
    - Da pag 51 tesi in poi: https://drive.google.com/drive/folders/1FErMZvfUOdZovVUGiWbAhy9e7mjsD1RP

<!---
- Accenno ai problemi aperti 
    - Prendere spunto da wikipedia
--> 

# 4. ALGORITMI GENERICI

- Algoritmo greedy base: http://www.geeksforgeeks.org/graph-coloring-set-2-greedy-algorithm/

- Miglioramento greedy Welsh-Powell: http://mrsleblancsmath.pbworks.com/w/file/fetch/46119304/vertex%20coloring%20algorithm.pdf

# 5. ESEMPIO DI APPLICAZIONE PRATICA

- Spiegazione gioco sudoku con esempio di gioco (facile, difficile): https://www.matematicamente.it/giochi-e-gare/sudoku/come-si-risolve-un-sudoku/

- Trasformazione del problema del sudoku in un problema di colorazione dei grafi: https://www.codeproject.com/Articles/801268/A-Sudoku-Solver-using-Graph-Coloring
    - http://www.cs.kent.edu/~dragan/ST-Spring2016/SudokuGC.pdf
    - VIDEO: https://www.youtube.com/watch?v=b-Q17dODWn4

- Implementazione algoritmo per risolverlo (pseudo-codice): https://github.com/CharlieC3/Sudoku/tree/master/src

- Analisi della complessità, tempo che ci impiega 

<!---
# 6. CONCLUSIONI

- Altri metodi di colorazione dei grafi( sottosezione wikipedia): https://en.wikipedia.org/wiki/Graph_coloring
- Accenni Teoria Ramsey (pag 109): https://drive.google.com/drive/folders/1FErMZvfUOdZovVUGiWbAhy9e7mjsD1RP
--> 
